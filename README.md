# Acts As Permissible

A simple solution for extending permissions to ActiveRecord models and objects.

#### Why would I need this gem?

Most applications will use an authorization library to enable role-based model access which is adequate in many cases. Other applications may require more granular control at the object-level thereby necessitating additional permissions specifications to partition these objects.

Acts As Permissible is not meant to replace any of these authorization libraries, but can augment functionality by allowing individual objects to have their own set of manageable permissions. Using this gem can expedite the setup of an extended and flexible permissions configuration.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'acts_as_permissible'
```

And then execute:

    $ bundle

Or install directly via:

    $ gem install acts_as_permissible

Next run the generator

    $ rails g acts_as_permissible:install

Finally, migrate the database changes

    $ rails db:migrate

## Usage

Acts As Permissible allows your ActiveRecord models to be categorized as permissible subjects or an objects.

Permissions can be granted to subjects and control access to objects.

Setting up permissions to a given model is as simple as adding the lines below to the respective set of subject and objects:

```ruby
class User < ActiveRecord::Base
  permissible_subject
end

class Article < ActiveRecord::Base
  permissible_object
end
```
Once we have designated our permissible models, we are ready to assign specific permissions to them.  Acts As Permissible offers some convenience methods to aid in assigning and checking permissions:

```ruby
# perform a simple permission check
user.can?(:read, article) # => false

# NOTE: Permission check will always return a boolean value, never nil
```
Unsurprisingly, our check returns false because we have not assigned any permissions.

Let's set our permissions.

### Grant Permission

```ruby
# grant permission to a subject (user in this case)
user.can!(:read, article)
```
We have now created a new permission record for the user to be permitted `read` privileges on the given article record.

```ruby
# our follow up permission check is successful
user.can?(:read, article) # => true
```
Granting a permission will create a new permission record in the database.

### Revoke Permission

```ruby
# Revoke permissions
user.cannot!(:read, article)

# Revoke global permissions
user.cannot!(:read)

```
Revoking a permission will destroy the permission record in the database.

### Global Permission Assignment

Permissions can also be designated for subjects without any corresponding permissible objects.  In this case, permissions can be checked and enforced against any named permissions without the need for an explicit object to be passed to it.

For instance, in the example below we want to control file downloading permissions at the group level, but do not care about restricting downloads of individual files.

```ruby
# perform a simple global permission check
group.can?(:download_files) # => false

# grant global permission to a subject
group.can!(:download_files)

```
### Convenience Methods

Acts As Permissible includes some basic helpers to facilitate permissions lookups. Some examples are shown below.

```ruby
# Retrieve all objects for which a subject has been granted read permissions
user.objects_with_permission(Article, :read)

# Retrieve all subjects granted read permissions to an object
article.subjects_with_permission(User, :read) 

```
These methods can also be passed an array to check permissions against a group.

```ruby
subject_array = [user_1, user_2]
object_array  = [article_1, article_2]

# Retrieve all objects for which a group of subjects has been granted read permissions
User.objects_with_permission(subject_array, :read) 

# Retrieve all subjects granted read permissions to a group of objects
Article.subjects_with_permission(object_array, User, :read) 

```

### Integration

Acts As Permissible works great with other authorization libraries such as [Pundit](https://github.com/elabs/pundit).

Here's an example of a basic Pundit policy class using a simple permission check against the currently authenticated user:

```ruby
# standalone policy
class ArticlePolicy
  attr_reader :user, :article

  def initialize(user, article)
    @user = user
    @article = article
  end

  def show?
    user.can?(:read, article)
  end
  
  def update?
    user.can?(:update, article)
  end
end

# inherited policy
class ArticlePolicy < ApplicationPolicy
  def show?
    user.can?(:read, record)
  end
  
  def update?
    user.admin? or user.can?(:update, record)
  end
end

# scoped policy
class ArticlePolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        user.objects_with_permission(Article, :read)
      end
    end
  end
  
  def index?
    true
  end
  
  def update?
    user.can?(:update, record)
  end
end
```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/ryleto/acts_as_permissible.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

