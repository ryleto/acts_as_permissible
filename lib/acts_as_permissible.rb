require 'acts_as_permissible/version'
require 'acts_as_permissible/exceptions'
require 'acts_as_permissible/model_extensions'
require 'acts_as_permissible/permission'

if defined? ActiveRecord::Base
  ActiveRecord::Base.send(:include, ActsAsPermissible::ModelExtensions)
end