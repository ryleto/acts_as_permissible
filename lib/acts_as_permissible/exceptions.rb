module ActsAsPermissible
  module Exceptions
    class NotAPermissibleObject < StandardError; end
    class NotAPermissibleSubject < StandardError; end
    class NotUniqueClasses < StandardError; end
  end
end