require 'acts_as_permissible/permissible_object'
require 'acts_as_permissible/permissible_subject'
require 'acts_as_permissible/permissible_all'

module ActsAsPermissible
  module ModelExtensions
    def self.included(base)
      base.send(:extend, ClassMethods)
      base.send(:include, ActsAsPermissible::ModelExtensions::All)
    end
  end

  module ClassMethods

    def permissible_object
      include ActsAsPermissible::ModelExtensions::Object
      extend ActsAsPermissible::ModelExtensions::Object::ClassMethods
      before_destroy :revoke_object_permissions
    end

    def permissible_subject
      include ActsAsPermissible::ModelExtensions::Subject
      extend ActsAsPermissible::ModelExtensions::Subject::ClassMethods
      before_destroy :revoke_subject_permissions 
    end

  end
end