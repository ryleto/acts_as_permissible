module ActsAsPermissible
  module ModelExtensions
    module All

      def permissible_subject?
        false
      end

      def permissible_object?
        false
      end

    end
  end
end