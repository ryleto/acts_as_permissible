module ActsAsPermissible
  module ModelExtensions
    module Object

      # Checks if the model is accessible by a given subject and a given permission
      # This is a proxy to subject.can?(permission, subject)
      #
      # @param subject [ActiveRecord::Base] The subject we are testing
      # @param permission [String, Symbol] The permission we want to test the subject on
      # @return [Boolean] True if the subject has the permission on this object, false otherwise
      def accessible_by?(subject, permission)
        subject.can?(permission, self)
      end

      # Gets the subjects that have the corresponding permission and type on this model
      #
      # @param type [Class] The type of the subjects we're looking for
      # @param permission [String, Symbol] The permission
      def subjects_with_permission(type, permission)
        type.joins("INNER JOIN obj_permissions ON obj_permissions.subject_id = #{type.table_name}.id").where('object_id = ? AND object_type = ?', self.id, self.class.to_s).where('subject_type = ?', type.to_s).where('permission_name = ?', permission)
      end

      # Revoke permissions for a given object
      def revoke_object_permissions
        ObjPermission.where(['object_id = ? AND object_type = ?', self.id, self.class.to_s]).destroy_all
      end

      def permissible_object?
        true
      end

      module ClassMethods
        # Gets the subjects that have the corresponding permission and type on this model
        #
        # params objects [Array or Object] The object(s) that you want subjects with permission
        # @param type [Class] The type of the subjects we're looking for
        # @param permission [String, Symbol] The permission
        def subjects_with_permission(objects, type, permission)
          class_name = (objects.is_a?(Array) ? objects.first.try(:class).try(:name) : objects.class.name)

          type.joins("INNER JOIN obj_permissions ON obj_permissions.subject_id = #{type.table_name}.id")
          .where(:'obj_permissions.object_type' => class_name)
          .where(:'obj_permissions.object_id' => objects)
          .where('subject_type = ?', type.to_s)
          .where('permission_name = ?', permission)
          .uniq
        end
      end
    end
  end
end