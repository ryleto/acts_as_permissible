module ActsAsPermissible
  module ModelExtensions
    module Subject

      # Grant permission to a given object
      #
      # @param permission [String, Symbol] The identifier of the permission
      # @param object [ActiveRecord::Base, nil] The model on which the permission is effective
      #   Can be nil if it is a global permission that does not target an object
      # @return [Bool] True if the role was successfully created, false if it was already present
      def can!(permission, object = nil)
        raise Exceptions::NotAPermissibleObject unless (object.nil? || object.permissible_object?)

        object_type = object.nil? ? nil : object.class.to_s
        object_id = object.nil? ? nil : object.id

        ObjPermission.new({
          :subject_id       => self.id,
          :subject_type     => self.class.to_s,
          :object_type      => object_type,
          :object_id        => object_id,
          :permission_name  => permission
          }).save
      end

      # Checks if the subject has a given permission on a given object
      #
      # @param permission [String, Symbol] The identifier of the permission
      # @param object [ActiveRecord::Base, nil] The model we are testing the permission on
      #   Can be nil if it is a global permission that does not target an object
      # @return [Bool] True if the user has the given permission, false otherwise
      def can?(permission, object = nil)
        raise Exceptions::NotAPermissibleObject unless (object.nil? || object.permissible_object?)
        ObjPermission.find_permission(self, object, permission) != nil
      end

      # Removes a permission on a given object
      #
      # @param permission [String, Symbol] The identifier of the permission
      # @param object [ActiveRecord::Base, nil] The model on which the permission is effective. Can be nil if it is a global permission that does not target an object
      # @return [Bool] True if the role was successfully removed, false if it did not exist
      def cannot!(permission, object = nil)
        permission = ObjPermission.find_permission(self, object, permission)
        return false if permission.nil?
        permission.destroy and return true
      end

      # Checks if the subject does not have a given permission on a given object
      # Acts as a proxy of !subject.can?(permission, object)
      #
      # @param permission [String, Symbol] The identifier of the permission
      # @param object [ActiveRecord::Base] The model we are testing the permission on. Can be nil if it is a global permission that does not target an object
      # @return [Bool] True if the user has not the given permission, false otherwise
      def cannot?(permission, object = nil)
        !self.can?(permission, object)
      end

      # Revoke permissions for a given subject
      def revoke_subject_permissions
        ObjPermission.where(['subject_id = ? AND subject_type = ?', self.id, self.class.to_s]).destroy_all
      end

      # Gets All objects that match a given type and permission
      #
      # @param type [Class] The type of the objects
      # @param permission [String, Symbol] The name of the permission
      # @return The macthing objects in an array
      def objects_with_permission(type, permission)
        type.joins("INNER JOIN obj_permissions ON obj_permissions.object_id = #{type.table_name}.id").where('object_type = ? AND subject_id = ?', type.to_s, self.id).where('permission_name = ?', permission)
      end

      def permissible_subject?
        true
      end

      module ClassMethods
        # Gets All objects that match a given type and permission
        #
        # params subjects [Array or Object] The subject(s) that you want objects with permission
        # @param type [Class] The type of the objects
        # @param permission [ String, Symbol] The name of the permission
        # @return The macthing objects in an array
        def objects_with_permission(subjects, type, permission)
          type.joins("INNER JOIN obj_permissions ON obj_permissions.object_id = #{type.table_name}.id")
          .where('object_type = ?', type.to_s)
          .where(:'obj_permissions.subject_id' => subjects)
          .where('permission_name = ?', permission)
          .uniq
        end
      end
    end
  end
end