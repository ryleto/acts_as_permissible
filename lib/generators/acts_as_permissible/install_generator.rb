require 'rails/generators/migration'

module ActsAsPermissible
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      
      include Rails::Generators::Migration
      
      def self.source_root
        @source_root ||= File.join(File.dirname(__FILE__), 'templates')
      end

      def self.next_migration_number(path)
        Time.now.utc.strftime("%Y%m%d%H%M%S")
      end
      
      def create_permissions_migration
        migration_template "create_obj_permissions.rb", "db/migrate/create_obj_permissions.rb"
      end
      
    end
  end
end
