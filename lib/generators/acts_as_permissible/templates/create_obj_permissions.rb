class CreateObjPermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :obj_permissions do |t|
      t.integer :subject_id
      t.string  :subject_type
      t.integer :object_id
      t.string  :object_type
      t.string  :permission_name
      t.timestamps
    end
    add_index :obj_permissions, :subject_id, name: 'subject_id_ix'
    add_index :obj_permissions, :object_id, name: 'object_id_ix'
  end
end